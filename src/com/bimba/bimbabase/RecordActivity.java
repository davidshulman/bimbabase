package com.bimba.bimbabase;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.media.MediaRecorder;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class RecordActivity extends Activity {
    
    private MediaRecorder recorder;
    private Button start, stop;
    File path;
    private String recordedTempPath;
    private String savedAudioFilePath;
    File destinationAudioFile;
    File recordedTempAudioFile;
    
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.audio_record_activity_layout);
        
        start = (Button)findViewById(R.id.startButton);
        start.setOnClickListener(startListener);
        stop = (Button)findViewById(R.id.saveButton);
        stop.setOnClickListener(stopListener);
        
        recorder = new MediaRecorder();
        //path = new File(Environment.getExternalStorageDirectory(),"myRecording.3gp");
        
        
        savedAudioFilePath =  this.getFilesDir().getAbsolutePath() + "/" + getResources().getString(R.string.string_key_audio_file);
		 destinationAudioFile = new File(savedAudioFilePath);
		 recordedTempAudioFile = new File(Environment.getExternalStorageDirectory(),"save_rec.3gp");
        
        
        resetRecorder();
    }
    
    @Override
    public void onDestroy() {
        super.onDestroy();
        recorder.release();
    }
    
    private void resetRecorder() {
        recorder.setAudioSource(MediaRecorder.AudioSource.MIC);
        recorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
        recorder.setAudioEncoder(MediaRecorder.AudioEncoder.DEFAULT);
        recorder.setOutputFile(recordedTempAudioFile.getAbsolutePath());
        try {
            recorder.prepare();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    private View.OnClickListener startListener = new View.OnClickListener() {     
        @Override
        public void onClick(View v) {
            try {
                recorder.start();
                
                start.setEnabled(false);
                stop.setEnabled(true);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    };
    
    private View.OnClickListener stopListener = new View.OnClickListener() {     
        @Override
        public void onClick(View v) {
            recorder.stop();
       
            resetRecorder();
            
            if (recordedTempAudioFile.exists())
			{
				try {

					InputStream in = new FileInputStream(recordedTempAudioFile.getPath());
					OutputStream out = new FileOutputStream(destinationAudioFile.getPath());

					// Copy the bits from instream to outstream
					byte[] buf = new byte[1024];
					int len;
					while ((len = in.read(buf)) > 0) {
						out.write(buf, 0, len);
					}
					in.close();
					out.close();

					if (destinationAudioFile.exists())
					{
						SharedPreferences sharedPref = getPreferences(Context.MODE_PRIVATE);
						SharedPreferences.Editor editor = sharedPref.edit();
						editor.putString(getString(R.string.string_key_audio_file), Uri.fromFile(destinationAudioFile).toString());
						editor.commit();
						Toast.makeText(RecordActivity.this, "Audio File was copied to : "  + destinationAudioFile.getPath(), Toast.LENGTH_LONG).show();
					}

				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}
            start.setEnabled(true);
            stop.setEnabled(false);
                        
            
        }
    };
}