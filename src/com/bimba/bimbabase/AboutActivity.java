package com.bimba.bimbabase;




import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnCreateContextMenuListener;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class AboutActivity extends Activity {

	protected static final int DIALOG_YES_NO_LONG_MESSAGE = 2;
	private Button btnOpenEULADialog;
	private TextView txtVersionNumber;
	private int NOTIFICATION = R.layout.about_activity_layout;
	private Menu mMenu;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub

		setContentView(R.layout.about_activity_layout);

		txtVersionNumber = (TextView)findViewById(R.id.textViewVersionNumber);
		txtVersionNumber.setText( "Application version : " + getVersion()  , TextView.BufferType.NORMAL);
		btnOpenEULADialog = (Button)findViewById(R.id.btnEULADialog);
		btnOpenEULADialog.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				showDialog(DIALOG_YES_NO_LONG_MESSAGE);
			}
		});			

		if (!AlarmBroadcastReceiver.isMyServiceRunning(this.getBaseContext())){

			startService(new Intent(AboutActivity.this,
					BootService.class));

			// launch our Boot service
			Intent serviceLocation = new Intent(AboutActivity.this, TrackerService.class);
			startService(serviceLocation);

		}

		//showStartNotification();
		
		showDialog(DIALOG_YES_NO_LONG_MESSAGE);
		super.onCreate(savedInstanceState);
	}


	public boolean onCreateOptionsMenu(Menu menu) {
		// Hold on to this
		mMenu = menu;

		// Inflate the currently selected menu XML resource.
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.main, menu);



		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.action_settings:
			Toast.makeText(this, "Show settibgs ", Toast.LENGTH_SHORT).show();

			Intent pIntentSettings = new Intent(this, DefaultValues.class);
			pIntentSettings.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			this.startActivity(pIntentSettings);
			return true;
			// Generic catch all for all the other menu resources
		default:
			// Don't toast text when a submenu is clicked
			if (!item.hasSubMenu()) {
				Toast.makeText(this, item.getTitle(), Toast.LENGTH_SHORT).show();
				return true;
			}
			break;
		}

		return false;
	}







	private void showStartNotification() {

		try {
			// Set the icon, scrolling text and timestamp

			PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, new Intent(this, AboutActivity.class),PendingIntent.FLAG_UPDATE_CURRENT) ;

			// Build notification
			Resources res = this.getResources();

			Notification notification = new NotificationCompat.Builder(this)
			.setContentTitle(res.getString( R.string.bimba_is_running))
			.setSmallIcon(R.drawable.b_toolbar_icon_bimba_32)
			.addAction(R.drawable.bimba_android_icon_32, res.getString( R.string.about_bimba), pendingIntent)
			.setContentIntent(pendingIntent).build();

			NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);

			// Send the notification.
			//notification.flags |= Notification.FLAG_AUTO_CANCEL;
			notificationManager.notify( NOTIFICATION , notification);

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private String getVersion() {
		String version = "0";
		try {
			PackageInfo pInfo = getPackageManager().getPackageInfo(getPackageName(), PackageManager.GET_META_DATA);
			version = pInfo.versionName;
		} catch (NameNotFoundException e1) {
			Log.e(this.getClass().getSimpleName(), "Name not found", e1);
		}
		return version;
	}


	public boolean onTouchEvent(MotionEvent event){
		this.finish();
		return true;
	}       


	@Override
	protected Dialog onCreateDialog(int id)
	{
		return new AlertDialog.Builder(AboutActivity.this)
		//.setIconAttribute(android.R.attr.alertDialogIcon)
		.setTitle(R.string.alert_dialog_two_buttons_msg)
		.setMessage(R.string.alert_dialog_two_buttons2_msg)
		.setPositiveButton(R.string.alert_dialog_ok, new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int whichButton) {

				/* User clicked OK so do some stuff */
			}
		})
		.setPositiveButton(R.string.alert_dialog_ok, new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int whichButton) {

				/* User clicked Cancel so do some stuff */
			}
		})
		.create();
	}

}
