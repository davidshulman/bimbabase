package com.bimba.bimbabase;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.Toast;

/*
 * Activity shall show dialog, play alert sound
 */

public class MainAlertActivity extends Activity {

	private Handler mMessageHandler;
	public static final int SHOW_ALERT  = 1;
	private  MediaPlayer mMediaPlayer = null;
	private ImageView imageV = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {

		getWindow().addFlags(WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON);
		getWindow().addFlags(WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED);
		getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

		setContentView(R.layout.main_alert_activity_layout);

		Bundle extras = getIntent().getExtras();
		if (extras == null) {
			return;
		}

		int value = extras.getInt(BTDeviceBroadcastReceiver.SHOW_ALERT);
//		String activeImagePath =  this.getFilesDir().getAbsolutePath() + "/" +
//									getResources().getString(R.string.string_key_imgae_file);
//		
		//String activeImagePath =  
		SharedPreferences sharedPref = getPreferences(Context.MODE_PRIVATE);

//		if (BuildConfig.DEBUG)
//			Log.e(GlobalConstants.APP_TAG , "BMP file to load " + activeImagePath );

		Bitmap bmp = null; 
//
//		File bitmapToShow = new File(activeImagePath);
//
//		if (bitmapToShow.exists())
//		{
//			try {
//				bmp=BitmapFactory.decodeStream(new FileInputStream(activeImagePath));
//			} catch (FileNotFoundException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}
//
//			if(value == GlobalConstants.ALERT_WITH_SNOOZE)
//				playSound();
//		}
		
			// use default resource
		
			if (value == GlobalConstants.ALERT_WITH_SNOOZE ) 
			{
				// Do something with the data
				playSound();
				bmp = BitmapFactory.decodeResource(getResources(), R.drawable.alert_banner);
			}		
			else
			{
				bmp = BitmapFactory.decodeResource(getResources(), R.drawable.splash_screen);
				showDialog(GlobalConstants.ASK_BABY_IN_CAR);
			}
		

		this.imageV = (ImageView)this.findViewById(R.id.imageViewPicture);
		imageV.setImageBitmap(bmp);

		super.onCreate(savedInstanceState);
		//finish();
	}


	@Override
	protected Dialog onCreateDialog(int id) {
		// TODO Auto-generated method stub
		switch (id)
		{
		case GlobalConstants.ALERT_WITH_SNOOZE:
		{
			return new AlertDialog.Builder(MainAlertActivity.this)
			.setIconAttribute(android.R.attr.alertDialogIcon)
			.setTitle("WARNING!!!")
			.setPositiveButton(R.string.alert_repeat_message, new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int whichButton) {
					/* User clicked OK so do some stuff */
				}
			})
			.setNegativeButton(R.string.alert_repeat_message , new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int whichButton) {
					/* User clicked Cancel so do some stuff */
				}
			})
			.create();
		}

		case GlobalConstants.ASK_BABY_IN_CAR:
		{
			//TODO dialog with timer !!!
			return new AlertDialog.Builder(MainAlertActivity.this)
			.setIconAttribute(android.R.attr.alertDialogIcon)
			.setTitle(R.string.dialog_isbaby_in_car)
			.setPositiveButton(R.string.string_yes, new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int whichButton) {

					/* User clicked OK so do some stuff */

					SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(MainAlertActivity.this); 
					Editor edit = preferences.edit();
					edit.putString("ASK_BABY_IN_CAR", "yes");
					edit.commit(); 
					
					MainAlertActivity.this.finish();
				}
			})
			.setNegativeButton(R.string.string_no , new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int whichButton) {

					/* User clicked Cancel so do some stuff */
					SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(MainAlertActivity.this); 
					Editor edit = preferences.edit();
					edit.putString("ASK_BABY_IN_CAR", "no");
					edit.commit(); 
					MainAlertActivity.this.finish();
				}
			})
			.create();
		}
		default:
		}

		return super.onCreateDialog(id);
	}

	private Handler handler = new Handler() {
		public void handleMessage(Message message) {
			Object path = message.obj;
			if (message.arg1 == SHOW_ALERT ) {
				Toast.makeText(MainAlertActivity.this,
						"Show alert and play sound !!! ", Toast.LENGTH_SHORT)
						.show();
				playSound();
				showDialog(SHOW_ALERT);

			} else {
				Toast.makeText(MainAlertActivity.this, "Download failed.",
						Toast.LENGTH_LONG).show();
			}

		};
	};


	private void playSound()
	{
		// check if custom file is used 
		String activeAudioPath =  this.getFilesDir().getAbsolutePath() + "/" + getResources().getString(R.string.string_key_audio_file);
		File customFile  = new File(activeAudioPath);
		if (customFile.exists())
		{
			mMediaPlayer =  MediaPlayer.create(this, Uri.fromFile(customFile));
		}
		else
		{
			mMediaPlayer = MediaPlayer.create(this, R.raw.baby_crying_alarm);
		}

		mMediaPlayer.setOnCompletionListener(new OnCompletionListener() {
			@Override
			public void onCompletion(MediaPlayer mp) {
				// TODO Auto-generated method stub
				MainAlertActivity.this.finish();
			}
		});

		try {
			mMediaPlayer.prepare();
		} catch (IllegalStateException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		mMediaPlayer.start();
	}

	protected void onDestroy() {
		super.onDestroy();
		// TODO Auto-generated method stub
		if (mMediaPlayer != null) {
			mMediaPlayer.release();
			mMediaPlayer = null;
		}

	}

}
