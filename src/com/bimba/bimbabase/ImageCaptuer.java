package com.bimba.bimbabase;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ApplicationInfo;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

public class ImageCaptuer extends Activity {

	private static final int REQUEST_IMAGE = 100;
	private static final int IMAGE_PICK = 1;
	private String capturedImagePath;

	Button captureButton;
	Button saveButton; 
	Button selectFileButton;
	ImageView imageView;
	File destination;
	File destinationTemp;
	FileOutputStream fos;

	private static final int REQUEST_AUDIO = 1;
	private static final int REQUEST_VIDEO = 2; 

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.image_capture_activity_layout);

		captureButton = (Button)findViewById(R.id.capture);
		captureButton.setOnClickListener(listener);

		saveButton = (Button)findViewById(R.id.btnSaveImage);
		saveButton.setOnClickListener(saveListner);

		selectFileButton = (Button) findViewById(R.id.btnSelectStoredFile);
		selectFileButton.setOnClickListener(slectfileListner); 
		imageView = (ImageView)findViewById(R.id.image);

		capturedImagePath =  this.getFilesDir().getAbsolutePath() + "/" + getResources().getString(R.string.string_key_imgae_file);
		destination = new File(capturedImagePath);
		destinationTemp = new File(Environment.getExternalStorageDirectory(),"save_image.jpg");

	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if(requestCode == REQUEST_IMAGE && resultCode == Activity.RESULT_OK) {
			//Bitmap userImage = (Bitmap)data.getExtras().get("data");
			try {
				FileInputStream in = new FileInputStream(destinationTemp);
				BitmapFactory.Options options = new BitmapFactory.Options();
				options.inSampleSize = 5; //Downsample 10x
				Bitmap userImage = BitmapFactory.decodeStream(in, null, options);
				imageView.setImageBitmap(userImage);

			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		if (requestCode ==  IMAGE_PICK) {
			if (resultCode == Activity.RESULT_OK) {
				// Do something with the data...
				//destinationTemp 
				Uri selectedImageUri = data.getData();

				String[] projection = { MediaStore.Images.Media.DATA}; 
				Cursor cursor = managedQuery(selectedImageUri, projection, null, null, null); 
				int column_index_data = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA); 
				cursor.moveToFirst(); 
				String seelctedImageFilePath = cursor.getString(column_index_data);
				loadFileImage(seelctedImageFilePath);

				if (destination.exists())
				{
					SharedPreferences sharedPref = getPreferences(Context.MODE_PRIVATE);
					SharedPreferences.Editor editor = sharedPref.edit();
					editor.putString(getString(R.string.string_key_imgae_file), Uri.fromFile(destination).toString());
					editor.commit();
				}

			} 
		}
	}


	private View.OnClickListener slectfileListner = new View.OnClickListener() {
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			Intent intent = new Intent();
			intent.setType("image/*");
			intent.setAction(Intent.ACTION_GET_CONTENT);
			startActivityForResult(Intent.createChooser(intent,"Select Image file"), IMAGE_PICK);


		}
	};


	private View.OnClickListener listener = new View.OnClickListener() {
		@Override
		public void onClick(View v) {

			Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
			//Add extra to save full-image somewhere
			intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(destinationTemp ));
			startActivityForResult(intent, REQUEST_IMAGE);

		}
	};

	private View.OnClickListener saveListner = new View.OnClickListener() {

		@Override
		public void onClick(View v)
		{
			copyImaheFile2Private(destination.getPath());

			if (destination.exists())
			{
				SharedPreferences sharedPref = getPreferences(Context.MODE_PRIVATE);
				SharedPreferences.Editor editor = sharedPref.edit();
				editor.putString(getString(R.string.string_key_imgae_file), Uri.fromFile(destination).toString());
				editor.commit();
			}
		}
	};


	private boolean copyImaheFile2Private (String imageFile2Copy)
	{
		boolean retVal=false;


		if (destinationTemp.exists())
		{
			try {

				InputStream in = new FileInputStream(destinationTemp.getPath());
				OutputStream out = new FileOutputStream(destination.getPath());

				// Copy the bits from instream to outstream
				byte[] buf = new byte[1024];
				int len;
				while ((len = in.read(buf)) > 0) {
					out.write(buf, 0, len);
				}
				in.close();
				out.close();

				if (destination.exists())
				{
					SharedPreferences sharedPref = getPreferences(Context.MODE_PRIVATE);
					SharedPreferences.Editor editor = sharedPref.edit();
					editor.putString(getString(R.string.string_key_imgae_file), Uri.fromFile(destination).toString());
					editor.commit();
					Toast.makeText(ImageCaptuer.this, "File was copied to : "  + destination.getPath(), Toast.LENGTH_LONG).show();
				}

			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}


		return retVal;
	}


	private void loadFileImage(String seelctedImageFilePath)
	{
		destinationTemp =  new File(seelctedImageFilePath);

		if (destinationTemp.exists())
		{
			Toast.makeText(ImageCaptuer.this, "User selected file: " + destinationTemp.getPath(), Toast.LENGTH_LONG).show();

			try {
				FileInputStream in = new FileInputStream(destinationTemp);
				BitmapFactory.Options options = new BitmapFactory.Options();
				options.inSampleSize = 5; //Downsample 10x
				Bitmap userImage = BitmapFactory.decodeStream(in, null, options);
				imageView.setImageBitmap(userImage);
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

	}

	
	public class ImageListRowItem
	{
		public ImageListRowItem()
		{
			
		}
	}
	
	
	private static class CustomImageListAdapter extends ArrayAdapter<ImageListRowItem>{

		public CustomImageListAdapter(Context context, int resource,
				int textViewResourceId, List<ImageListRowItem> objects) {
			super(context, resource, textViewResourceId, objects);
			// TODO Auto-generated constructor stub
		}	
		
}
