package com.bimba.bimbabase;

// development only , dont forget to remove 

import java.io.IOException;

import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.SystemClock;
import android.app.Activity;
import android.app.AlarmManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.util.Log;
import android.view.Menu;
import android.widget.Button;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.res.Resources;
import android.os.IBinder;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Toast;
import com.bimba.bimbabase.BootService;

public class MainActivity extends Activity {
	private int NOTIFICATION = R.layout.about_activity_layout;
	private  MediaPlayer mMediaPlayer = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_main);

		// Watch for button clicks.
		Button button = (Button)findViewById(R.id.toggleServiceButton);
		button.setOnClickListener(mStartListener);
		button = (Button)findViewById(R.id.testbutton);
		button.setOnClickListener(mtestButtonListner);


		button = (Button)findViewById(R.id.buttonNotify);
		button.setOnClickListener(mnotifyButtonListner);
	}

	private OnClickListener mStartListener = new OnClickListener() {
		public void onClick(View v) {
			// Make sure the service is started.  It will continue running
			// until someone calls stopService().  The Intent we use to find
			// the service explicitly specifies our service component, because
			// we want it running in our own process and don't want other
			// applications to replace it.

			//TODO
			startService(new Intent(MainActivity.this,
					BootService.class));
			
			
			
			
			
			
		}
	};

	private OnClickListener mtestButtonListner = new OnClickListener() {
		public void onClick(View v) {

			Intent i = new Intent(MainActivity.this, AboutActivity.class);
			startActivity(i); 

			playSound();

		}

	};

	
	

	private OnClickListener mnotifyButtonListner = new OnClickListener() {
		public void onClick(View v) {

			showStartNotification();

		}

	};



	private void showStartNotification() {

		try {
			// Set the icon, scrolling text and timestamp

			PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, new Intent(this, AboutActivity.class),PendingIntent.FLAG_UPDATE_CURRENT) ;

			// Build notification
			Resources res = this.getResources();
			// Actions are just fake
			Notification notification = new Notification.Builder(this)
			.setContentTitle(res.getString( R.string.bimba_is_running))
			.setSmallIcon(R.drawable.b_toolbar_icon_bimba_32)
			.addAction(R.drawable.bimba_android_icon_32, res.getString( R.string.about_bimba), pendingIntent)
			.setContentIntent(pendingIntent).build();

			NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);

			// Send the notification.
			//notification.flags |= Notification.FLAG_AUTO_CANCEL;
			notificationManager.notify( NOTIFICATION , notification);

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	
	
	private void playSound()

	{
		mMediaPlayer = MediaPlayer.create(this, R.raw.elarm);
		try {
			mMediaPlayer.prepare();
		} catch (IllegalStateException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		mMediaPlayer.start();
	}

	protected void onDestroy() {
		super.onDestroy();
		// TODO Auto-generated method stub
		if (mMediaPlayer != null) {
			mMediaPlayer.release();
			mMediaPlayer = null;
		}

	}

}




