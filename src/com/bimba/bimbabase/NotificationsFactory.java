package com.bimba.bimbabase;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Build;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.content.Context;


public class NotificationsFactory {
	private Context mctx = null;
	//private NotificationManager mNotifManager = null;


	/**
	 * Show a notification while this service is running.
	 * simple way , runs in the main thread
	 */
	@SuppressWarnings("deprecation")
	public void showPlainStartNotification(Context callingContetx) {

		NotificationManager mNotifManager = (NotificationManager)callingContetx.getSystemService(Context.NOTIFICATION_SERVICE);
		try {

			PendingIntent pIntent = PendingIntent.getActivity(callingContetx, 0, new Intent(callingContetx, AboutActivity.class),PendingIntent.FLAG_UPDATE_CURRENT) ;
			PendingIntent pIntentSettings = PendingIntent.getActivity(callingContetx, 0, new Intent(callingContetx, DefaultValues.class),PendingIntent.FLAG_UPDATE_CURRENT) ;

			// Build notification
			Resources res = callingContetx.getResources();

			// Actions are just fake
			NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(callingContetx )
			.setSmallIcon(R.drawable.b_toolbar_icon_bimba_32);

			if(Build.VERSION.SDK_INT  >= 11)
			{
				notificationBuilder.addAction(R.drawable.bimba_android_icon_32, res.getString( R.string.about_bimba), pIntent);	
				notificationBuilder.addAction(R.drawable.settings_32, res.getString(R.string.notification_settings), pIntentSettings);	
			}
			else
			{
				notificationBuilder.setContentIntent(pIntent);
			}


			notificationBuilder.setContentTitle(res.getString( R.string.app_name));
			notificationBuilder.setContentText(res.getString(R.string.bimba_is_running));
			Notification notification = notificationBuilder.build();  
			notification.flags = Notification.FLAG_NO_CLEAR;
			mNotifManager.notify(GlobalConstants.NOTIFICATION,notification);


		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}


	/*
	 *
	 * 
	 */
	@SuppressWarnings("deprecation")
	public void showBTDisabledNotification(Context callingContetx) {

		NotificationManager mNotifManager = (NotificationManager)callingContetx.getSystemService(Context.NOTIFICATION_SERVICE);
		try {

			PendingIntent pIntent = PendingIntent.getActivity(callingContetx, 0, new Intent(callingContetx, AboutActivity.class),PendingIntent.FLAG_UPDATE_CURRENT) ;
			PendingIntent pIntentSettings = PendingIntent.getActivity(callingContetx, 0, new Intent(callingContetx, DefaultValues.class),PendingIntent.FLAG_UPDATE_CURRENT) ;

			// Build notification
			Resources res = callingContetx.getResources();

			// Actions are just fake
			NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(callingContetx )
			.setSmallIcon(R.drawable.b_toolbar_icon_bimba_32);

			notificationBuilder.setContentIntent(pIntent);
			notificationBuilder.setContentTitle(res.getString( R.string.app_name));
			// update notification text
			notificationBuilder.setContentText(res.getString(R.string.notification_enable_bt));
			Notification notification = notificationBuilder.build();  
			notification.flags = Notification.FLAG_NO_CLEAR;
			mNotifManager.notify(GlobalConstants.NOTIFICATION,notification);


		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		mNotifManager = null;
	}





	/**
	 * 
	 */
	@SuppressWarnings("deprecation")
	public void showLocationsDisabledNotification(Context callingContetx) {

		NotificationManager mNotifManager = (NotificationManager)callingContetx.getSystemService(Context.NOTIFICATION_SERVICE);

		try {

			PendingIntent pIntent = PendingIntent.getActivity(callingContetx, 0, new Intent(callingContetx, AboutActivity.class),PendingIntent.FLAG_UPDATE_CURRENT) ;
			PendingIntent pIntentSettings = PendingIntent.getActivity(callingContetx, 0, new Intent(callingContetx, DefaultValues.class),PendingIntent.FLAG_UPDATE_CURRENT) ;

			// Build notification
			Resources res = callingContetx.getResources();

			// Actions are just fake
			NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(callingContetx )
			.setSmallIcon(R.drawable.b_toolbar_icon_bimba_32);

			notificationBuilder.setContentIntent(pIntent);
			notificationBuilder.setContentTitle(res.getString( R.string.app_name));
			// update notification text
			notificationBuilder.setContentText(res.getString(R.string.notification_enable_gps));
			Notification notification = notificationBuilder.build();  
			notification.flags = Notification.FLAG_NO_CLEAR;
			mNotifManager.notify(GlobalConstants.NOTIFICATION,notification);


		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		mNotifManager = null;
	}



}
