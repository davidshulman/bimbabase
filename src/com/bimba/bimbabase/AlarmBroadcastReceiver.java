package com.bimba.bimbabase;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import android.app.ActivityManager;
import android.app.ActivityManager.RunningServiceInfo;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.text.format.DateFormat;
import android.util.Log;
import android.widget.Toast;

public class AlarmBroadcastReceiver extends BroadcastReceiver {

	@Override
	public void onReceive(Context context, Intent intent) {
		// TODO Auto-generated method stub

		Log.d(GlobalConstants.APP_TAG, "AlarmBroadcastReceiver called");

		try {
			if (!isMyServiceRunning(context))
			{
				if(BuildConfig.DEBUG) 
				{
					Log.d(GlobalConstants.APP_TAG, "AlarmBroadcastReceiver would start BootService");
					Toast.makeText(context, "AlarmBroadcastReceiver would start BootService",Toast.LENGTH_SHORT).show();
				}

				// launch our Boot service
				Intent service = new Intent(context, BootService.class);
				context.startService(service);
				
				
				// launch our Boot service
				Intent serviceLocation = new Intent(context, TrackerService.class);
				context.startService(serviceLocation);
				
				
				
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}


	public static boolean isMyServiceRunning( Context context ) {
		ActivityManager manager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
		for (RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
			if (BootService.class.getName().equals(service.service.getClassName())) {
				return true;
			}
		}
		return false;
	}
	
	
//	
//	public static boolean isMyLocationServiceRunning( Context context ) {
//		ActivityManager manager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
//		for (RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
//			if (BootService.class.getName().equals(service.service.getClassName())) {
//				return true;
//			}
//		}
//		return false;
//	}





}
