package com.bimba.bimbabase;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Set;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.os.Build;
import android.util.Log;
import android.widget.ArrayAdapter;

public class BTDevicesContainer {

	public static ArrayList<BluetoothDevice> devicesList;
	private static BluetoothDevice mDevice;
	private static ArrayList<String> mDevicesHistory;
	public static final ArrayList<String> getmDevicesHistory() {
		return mDevicesHistory;
	}

	public static final ArrayList<String> getmPairedDevices() {
		return mPairedDevices;
	}


	private static ArrayList<String> mPairedDevices;
	static
	{
		mDevicesHistory  =  new ArrayList<String>();
		mPairedDevices =  new ArrayList<String>();

	}


	public BTDevicesContainer()
	{

	}


	public static void  addNewDevice(String strDescription)
	{

	}


	public static void scanPairedDevies()
	{

		BluetoothAdapter mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

		Set<BluetoothDevice> pairedDevices = mBluetoothAdapter.getBondedDevices();
		// If there are paired devices
		if (pairedDevices.size() > 0) {
			// Loop through paired devices
			for (BluetoothDevice device : pairedDevices) {
				// Add the name and address to an array adapter to show in a ListView
				//mPairedDevicesArrayAdapter.add(device.getName() + "\n" + device.getAddress());
				mDevicesHistory.add(device.getName());
				if(BuildConfig.DEBUG) 
					Log.d(GlobalConstants.APP_TAG, "Paired BT device: " + device.getAddress() + " " +  device.getName());
			}
		}

	}

}
