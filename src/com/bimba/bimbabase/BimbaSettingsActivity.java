package com.bimba.bimbabase;

import android.os.Bundle;
import android.preference.PreferenceActivity;

public class BimbaSettingsActivity extends PreferenceActivity {
	
	public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.bimba_prefernce_activity_layout);
    }

}
