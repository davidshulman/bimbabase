package com.bimba.bimbabase;

import java.util.ArrayList;
import java.util.Set;
import android.app.ActionBar;
import android.app.Activity;
import android.app.Application;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

public class HWInfoProvider {

	private BluetoothAdapter mBluetoothAdapter = null;
	// Member object for the chat services
	private Context mCtxRefernce  = null;
	private ArrayList<String> mPairedDevice = null;

	public HWInfoProvider(Context ctx)
	{
		mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
		mCtxRefernce =  ctx;
		mPairedDevice =  new ArrayList<String>();


	}


	public void getBTstatus()
	{

		// If the adapter is null, then Bluetooth is not supported
		if (mBluetoothAdapter == null) {
			Toast.makeText(mCtxRefernce, "Bluetooth is not available", Toast.LENGTH_LONG).show();

		}
		else
		{
			if (!mBluetoothAdapter.isEnabled()) {
				Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
				// TODO startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
			}
		}

	}


	private void getListOfPairedDevice()
	{
		// Get a set of currently paired devices
		Set<BluetoothDevice> pairedDevices = mBluetoothAdapter.getBondedDevices();

		// If there are paired devices, add each one to the ArrayAdapter
		if (pairedDevices.size() > 0) {

			for (BluetoothDevice device : pairedDevices) {
				// mPairedDevicesArrayAdapter.add(device.getName() + "\n" + device.getAddress());
				mPairedDevice.add(device.getName());

			}
		} else {

			//mPairedDevicesArrayAdapter.add(noDevices);
		}

	}

}
