package com.bimba.bimbabase;

import java.util.Calendar;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.text.InputFilter.LengthFilter;
import android.util.Log;
import android.widget.Toast;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.os.SystemClock;
import android.os.Vibrator;
import android.util.DebugUtils;

public class BootCompletedBroadcastReceiver extends BroadcastReceiver {

	private static final boolean D = true;
	private static final long ALARM_PERIOD =  60000; // Alarm once in a minute

	@Override
	public void onReceive(Context context, Intent intent) {
		// TODO Auto-generated method stub

		if(BuildConfig.DEBUG) 
			Log.d(GlobalConstants.APP_TAG, "BootCompletedBroadcastReceiver");

		// add alarm manager here
		registerForAlarmReceiver(context);

		// launch our Boot service
		Intent service = new Intent(context, BootService.class);
		context.startService(service);

	}


	public void registerForAlarmReceiver(Context context)
	{

		try {

			Intent intent = new Intent(context, AlarmBroadcastReceiver.class);
			PendingIntent pintent = PendingIntent.getBroadcast(context, 0, intent, 0);

			AlarmManager alarm = (AlarmManager)context.getSystemService(Context.ALARM_SERVICE);
			// check state 
			// Start every 300 seconds
			alarm.setRepeating(AlarmManager.RTC_WAKEUP, SystemClock.elapsedRealtime() + getAlarmPeriod(), getAlarmPeriod() , pintent);

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 

		if(BuildConfig.DEBUG) 
			Log.d(GlobalConstants.APP_TAG, "registerForAlarmReceiver succeeded");
	}


	public static long getAlarmPeriod() {
		return ALARM_PERIOD;
	}


}
