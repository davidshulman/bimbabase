package com.bimba.bimbabase;


import java.security.acl.NotOwnerException;
import java.sql.Date;

import android.app.AlertDialog;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.bluetooth.BluetoothAdapter;
import android.content.Intent;
import android.content.res.Resources;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.support.v4.app.NotificationCompat;
import android.text.format.DateFormat;
import android.text.format.DateUtils;
import android.text.format.Time;
import android.text.method.DateTimeKeyListener;
import android.util.Log;
import android.os.Process;
import android.provider.Settings;

public class BootService extends Service {

	private NotificationManager mNM;
	private BluetoothAdapter mBluetoothAdapter;

	// Unique Identification Number for the Notification.
	// We use it on Notification start, and to cancel it.

	//private Looper mServiceLooper;
	//private ServiceHandler mServiceHandler;


	@Override
	public void onCreate() {

		mNM = (NotificationManager)getSystemService(NOTIFICATION_SERVICE);

	}


	@Override
	public IBinder onBind(Intent arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		// TODO Auto-generated method stub

		Log.d(GlobalConstants.APP_TAG, "Boot Service is running. Intent info=: " + intent.getAction() +  " " + intent.getDataString());

		ProbeHWStatePeriodically sync = new ProbeHWStatePeriodically(spinWorkerThread,30*1000);
		mBluetoothAdapter  = BluetoothAdapter.getDefaultAdapter();

		if (mBluetoothAdapter!= null){
			testBTState();

			// TO DO wait for result !!!
			//showPlainStartNotification();
			NotificationsFactory nf =  new NotificationsFactory();
			nf.showPlainStartNotification(this);
		}
		else
		{
			///showPlainStartNotification();
		}

		return super.onStartCommand(intent, flags, startId);
	}

//	/**
//	 * Show a notification while this service is running.
//	 * simple way , runs in the main thread
//	 */
//	@SuppressWarnings("deprecation")
//	private void showPlainStartNotification() {
//
//		try {
//
//			PendingIntent pIntent = PendingIntent.getActivity(this, 0, new Intent(this, AboutActivity.class),PendingIntent.FLAG_UPDATE_CURRENT) ;
//			PendingIntent pIntentSettings = PendingIntent.getActivity(this, 0, new Intent(this, DefaultValues.class),PendingIntent.FLAG_UPDATE_CURRENT) ;
//
//			// Build notification
//			Resources res = this.getResources();
//
//			if(BuildConfig.DEBUG) 
//				Log.d(GlobalConstants.APP_TAG, "SDK veriosn is " +  Build.VERSION.SDK);
//
//			// Actions are just fake
//			NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
//
//			.setSmallIcon(R.drawable.b_toolbar_icon_bimba_32);
//
//			if(Build.VERSION.SDK_INT  >= 11)
//			{
//				notificationBuilder.addAction(R.drawable.settings_32, res.getString(R.string.notification_settings), pIntentSettings);	
//				notificationBuilder.addAction(R.drawable.bimba_android_icon_32, res.getString( R.string.about_bimba), pIntent);
//			}
//			else
//			{
//				notificationBuilder.setContentIntent(pIntent);
//			}
//			notificationBuilder.setContentTitle(res.getString( R.string.app_name));
//			notificationBuilder.setContentText(res.getString(R.string.bimba_is_running));
//			Notification notification = notificationBuilder.build();  
//			notification.flags = Notification.FLAG_NO_CLEAR;
//			mNM.notify(GlobalConstants.NOTIFICATION,notification);
//
//		} catch (Exception e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//	}

	final private Runnable spinWorkerThread = new Runnable() {
		public void run() {
			handler.postDelayed(spinWorkerThread,30*1000);

			Time today = new Time(Time.getCurrentTimezone());
			today.setToNow();
			Log.d(GlobalConstants.APP_TAG, "ProbeHWStatePeriodically is called  " + today.format("%k:%M:%S"));
			// now do real job and post message 
			testBTState();

			testLocationState();
		}
	};


	private boolean testBTState()
	{
		boolean retVal = false;

		if (!mBluetoothAdapter.isEnabled()) {
			Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
			enableBtIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			this.startActivity(enableBtIntent);
		}
		else
		{
			//BTDevicesContainer.scanPairedDevies();
		}

		return retVal;
	}


	private boolean testLocationState()
	{
		boolean retVal = false;
		LocationManager manager = (LocationManager)getSystemService(LOCATION_SERVICE);

		if(!manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {

			//				AlertDialog.Builder builder = new AlertDialog.Builder(this); 
			//				builder.setTitle("Location Manager");
			//				builder.setMessage("We want to use your location, but GPS is currently disabled.\n"	+"Would you like to change these settings now?");

			Intent intentGPSenabled = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS); 
			intentGPSenabled.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			startActivity(intentGPSenabled);
		}

		return retVal;
	}


	public final Handler handler = new Handler();

	public class ProbeHWStatePeriodically {
		Runnable task;

		public ProbeHWStatePeriodically(Runnable task, long time) {
			this.task = task;

			handler.removeCallbacks(task);
			handler.postDelayed(task, time);
			//handler.sendMessage( Message.obtain(orig)());
		}
	}

}
