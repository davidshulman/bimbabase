package com.bimba.bimbabase;

import java.util.ArrayList;

import android.app.AlertDialog;
import android.app.Service;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;
import android.widget.Toast;
import android.provider.Settings;

public class TrackerService extends Service implements LocationListener {

	private static final String LOGTAG = "TrackerService";

	private LocationManager manager;
	private ArrayList<Location> storedLocations;

	private boolean isTracking = false;

	/* Service Setup Methods */
	@Override
	public void onCreate() {
		manager = (LocationManager)getSystemService(LOCATION_SERVICE);
		storedLocations = new ArrayList<Location>();

		startTracking();
		Log.i(LOGTAG, "Tracking Service Running...");
	}

	public void startTracking() {
		if(!manager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {

			try {
				AlertDialog.Builder builder = new AlertDialog.Builder(this); 
				builder.setTitle("Location Manager");
				builder.setMessage("We want to use your location, but Network is currently disabled.\n"	+"Would you like to change these settings now?");

				Intent intentGPSenabled = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS); 
				intentGPSenabled.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				startActivity(intentGPSenabled);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			
			
			if (!manager.isProviderEnabled(LocationManager.PASSIVE_PROVIDER))
			{
				// 
				Intent intentGPSenabled = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS); 
				intentGPSenabled.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				startActivity(intentGPSenabled);
			}
		}


		Toast.makeText(this, "Starting Tracker", Toast.LENGTH_SHORT).show();
		manager.requestLocationUpdates(LocationManager.GPS_PROVIDER, GlobalConstants.LOCATION_REQUEST_INTERVAL , GlobalConstants.LOCATION_MIN_DISTANCE, this);

		isTracking = true;
	}

	public void stopTracking() {
		Toast.makeText(this, "Stopping Tracker", Toast.LENGTH_SHORT).show();
		manager.removeUpdates(this);
		isTracking = false;
	}

	public boolean isTracking() {
		return isTracking;
	}

	@Override
	public void onDestroy() {
		manager.removeUpdates(this);
		Log.i(LOGTAG, "Tracking Service Stopped...");
	}

	/* Service Access Methods */
	public class TrackerBinder extends Binder {
		TrackerService getService() {
			return TrackerService.this;
		}
	}

	private final IBinder binder = new TrackerBinder();

	@Override
	public IBinder onBind(Intent intent) {
		return binder;
	}

	public int getLocationsCount() {
		return storedLocations.size();
	}

	public ArrayList<Location> getLocations() {
		return storedLocations;
	}

	/* LocationListener Methods */
	@Override
	public void onLocationChanged(Location location) {
		Log.i("TrackerService", "Adding new location");
		storedLocations.add(location);
		//Toast.makeText(this, "Location changes: " +  location.toString(), Toast.LENGTH_LONG).show();
	}

	@Override
	public void onProviderDisabled(String provider) {
		
		//TODO
		// post notification Location was disabled  
		
		NotificationsFactory nF = new NotificationsFactory();
		nF.showBTDisabledNotification(this.getApplicationContext());
		MainApplication.GPSisOn = false;
	}

	@Override
	public void onProviderEnabled(String provider) { 
		MainApplication.GPSisOn = true;
		
		
	}

	@Override
	public void onStatusChanged(String provider, int status, Bundle extras) { }
}
