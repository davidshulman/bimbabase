package com.bimba.bimbabase;

import com.bimba.bimbabase.MainApplication.AlertStates;

import android.app.AlertDialog;
import android.app.KeyguardManager;
import android.app.Notification;
import android.app.PendingIntent;
import android.app.KeyguardManager.KeyguardLock;
import android.app.NotificationManager;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.res.Resources;
import android.os.Build;
import android.os.Messenger;
import android.os.PowerManager.WakeLock;
import android.os.Vibrator;
import android.preference.PreferenceManager;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.Toast;

public class BTDeviceBroadcastReceiver extends BroadcastReceiver {

	private Context mctx = null;
	BluetoothAdapter mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
	private ArrayAdapter<String> mArrayAdapter ;
	public static final String  SHOW_ALERT  = "SHOW_ALERT";
	// public static final String  SHOW_BABY_INCAR =  "SHOW_BABY_INCAR";

	public BTDeviceBroadcastReceiver()
	{
		// to do 
	}


	@Override
	public void onReceive(Context context, Intent in_intent) {

		mctx = context;

		String action = in_intent.getAction();

		// get pointer to device 
		BluetoothDevice device = in_intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);		

		if (BuildConfig.DEBUG) {
			Log.e(GlobalConstants.APP_TAG , "Action on BTDeviceBroadcastReceiver" +  action + " state: "  + mBluetoothAdapter.getState());
		} 


		if (mBluetoothAdapter.getState() == BluetoothAdapter.STATE_ON || 
				mBluetoothAdapter.getState() ==  BluetoothAdapter.STATE_TURNING_ON || 
				mBluetoothAdapter.getState() == BluetoothAdapter.STATE_CONNECTED || 
				mBluetoothAdapter.getState() == BluetoothAdapter.STATE_CONNECTING)
		{
			MainApplication.getInstance().BTisOn  =  true;

			if (BuildConfig.DEBUG) {
				Log.e(GlobalConstants.APP_TAG , "BT adapter is ON");
			} 

		}
		else
		{
			if (!mBluetoothAdapter.isEnabled()) {
				Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
				enableBtIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				mctx.startActivity(enableBtIntent);

				MainApplication.getInstance().BTisOn = false;

				if (BuildConfig.DEBUG) {
					Log.e(GlobalConstants.APP_TAG , "Requesting to enable BT -  adapter is OFF");
				} 
			}
			else
			{
				//TOOD
				BTDevicesContainer.scanPairedDevies();
			}
		}


		// When discovery finds a device
		if (BluetoothDevice.ACTION_FOUND.equals(action)) {
			// Get the BluetoothDevice object from the Intent

			// Add the name and address to an array adapter to show in a ListView
			//mArrayAdapter.add(device.getName() + "\n" + device.getAddress());

			Toast.makeText(context, "BT device ACTION_FOUND"  +device.getName() + "\n" + device.getAddress()  ,
					Toast.LENGTH_SHORT).show();
		}
		else if (BluetoothDevice.ACTION_ACL_DISCONNECTED.equals(action)) {
			Toast.makeText(context, "BT device ACTION_ACL_DISCONNECTED "  +device.getName() + "\n" + device.getAddress()  ,
					Toast.LENGTH_SHORT).show();

			// show alert when connection is lost, in case BT was turned off show notification message! 
			SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(mctx); 
			if(preferences.getString("ASK_BABY_IN_CAR", "yes").equalsIgnoreCase("yes"))
			{
				
				MainApplication.getInstance().AlertLevel = AlertStates.ALERT_0;
				showMainAlertMessage();
			}

		}
		else if (BluetoothDevice.ACTION_ACL_CONNECTED.equals(action)) {
			Toast.makeText(context, "BT device ACTION_ACL_CONNECTED "  +device.getName() + "\n" + device.getAddress()  ,
					Toast.LENGTH_SHORT).show();

			// device was paired 
			showAskUserAboutBbay();
			MainApplication.getInstance().BTisConnected = true;


		}
		else if (BluetoothDevice.ACTION_ACL_DISCONNECT_REQUESTED.equals(action)) {
			Toast.makeText(context, "BT device ACTION_ACL_DISCONNECT_REQUESTED "  +device.getName() + "\n" + device.getAddress()  ,
					Toast.LENGTH_SHORT).show();

		}


		//
		//		else if (mBluetoothAdapter.ACTION_REQUEST_ENABLE.equals(action)) {
		//			BluetoothDevice device = in_intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
		//			Toast.makeText(context, "BT adapter ACTION_REQUEST_ENABLE "  +device.getName() + "\n" + device.getAddress()  ,
		//					Toast.LENGTH_SHORT).show();
		//
		//		}
		//
		//		else if (mBluetoothAdapter.ACTION_DISCOVERY_FINISHED.equals(action)) {
		//			BluetoothDevice device = in_intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
		//			Toast.makeText(context, "BT adapter ACTION_DISCOVERY_FINISHED "  +device.getName() + "\n" + device.getAddress()  ,
		//					Toast.LENGTH_SHORT).show();
		//
		//		}
		//
		//		else if (BluetoothDevice.ACTION_FOUND.equals(action)) {
		//            // Get the BluetoothDevice object from the Intent
		//            BluetoothDevice device = in_intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
		//            // If it's already paired, skip it, because it's been listed already
		//            if (device.getBondState() != BluetoothDevice.BOND_BONDED) {
		//               // mNewDevicesArrayAdapter.add(device.getName() + "\n" + device.getAddress());
		//            }
		//            
		//
		//            Toast.makeText(context, "BluetoothDevice.ACTION_FOUND "  +device.getName() + "\n" + device.getAddress()  ,
		//					Toast.LENGTH_SHORT).show();
		//		}
		//		
		//		else if (BluetoothDevice.ACTION_ACL_DISCONNECTED.equals(action)) {
		//            // Get the BluetoothDevice object from the Intent
		//            BluetoothDevice device = in_intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
		//            // If it's already paired, skip it, because it's been listed already
		//            if (device.getBondState() != BluetoothDevice.BOND_BONDED) {
		//               // mNewDevicesArrayAdapter.add(device.getName() + "\n" + device.getAddress());
		//            	//showAlertDialog();
		//            }
		//            
		//            
		//            Toast.makeText(context, "BluetoothDevice.ACTION_ACL_DISCONNECTED "  +device.getName() + "\n" + device.getAddress()  ,
		//					Toast.LENGTH_SHORT).show();
		//            
		//            showNotificationSimple();
		//		}
		//		
		//		
		//		else if (BluetoothDevice.ACTION_ACL_CONNECTED.equals(action)) {
		//            // Get the BluetoothDevice object from the Intent
		//            BluetoothDevice device = in_intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
		//            // If it's already paired, skip it, because it's been listed already
		//            if (device.getBondState() != BluetoothDevice.BOND_BONDED) {
		//               // mNewDevicesArrayAdapter.add(device.getName() + "\n" + device.getAddress());
		//            	//showAlertDialog();
		//            }
		//            
		//            
		//            Toast.makeText(context, "BluetoothDevice.ACTION_ACL_CONNECTED "  +device.getName() + "\n" + device.getAddress()  ,
		//					Toast.LENGTH_SHORT).show();
		//            
		//            
		//            
		//		}
		//		
		//		
		//		else if (BluetoothDevice.ACTION_ACL_DISCONNECT_REQUESTED.equals(action)) {
		//            // Get the BluetoothDevice object from the Intent
		//            BluetoothDevice device = in_intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
		//            // If it's already paired, skip it, because it's been listed already
		//            if (device.getBondState() != BluetoothDevice.BOND_BONDED) {
		//               // mNewDevicesArrayAdapter.add(device.getName() + "\n" + device.getAddress());
		//            	//showAlertDialog();
		//            }
		//            
		//            
		//            Toast.makeText(context, "BluetoothDevice.ACTION_ACL_DISCONNECT_REQUESTED "  +device.getName() + "\n" + device.getAddress()  ,
		//					Toast.LENGTH_SHORT).show();
		//            
		//            
		//           
		//		}

		BTDevicesContainer.scanPairedDevies();
	}


	public void showAskUserAboutBbay()
	{

		try {
			Intent i = new Intent(mctx, MainAlertActivity.class);
			i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			i.putExtra(SHOW_ALERT, GlobalConstants.ASK_BABY_IN_CAR);
			mctx.startActivity(i);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}  
	}


/*
 * This shows main alert
 * 
 */
	public void showNotificationSimple()
	{
		try {
			Intent i = new Intent(mctx, MainAlertActivity.class);
			i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			i.putExtra(SHOW_ALERT, GlobalConstants.ALERT_WITH_SNOOZE);
			mctx.startActivity(i);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}  

	}


	public void showMainAlertMessage()
	{
		try {
			Intent i = new Intent(mctx, MainAlertActivity.class);
			i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			i.putExtra(SHOW_ALERT, GlobalConstants.ALERT_WITH_SNOOZE);
			mctx.startActivity(i);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}  

	}

	
	
	
	
	private void requestBTenable()
	{

		if (!mBluetoothAdapter.isEnabled()) {
			Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
			enableBtIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			mctx.startActivity(enableBtIntent);
		}
	}


}
