package com.bimba.bimbabase;

public interface GlobalConstants {

	String APP_TAG = "com.bimba.bimbabase";
	int VIBR_COUNT = 2;

	int ALERT_WITH_SNOOZE = 1;
	int ASK_BABY_IN_CAR = 2;
	int NOTIFICATION = R.layout.about_activity_layout;
	int LOCATION_REQUEST_INTERVAL = 120 *  1000;
	int LOCATION_MIN_DISTANCE = 20;
}
